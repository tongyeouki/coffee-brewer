from pprint import pprint

from config import RATIOS


class Ratio:
    def __init__(self, ratio: str):
        self.denominator = None
        self.numerator = None
        self.ratio = ratio

    def convert(self):
        r = [float(x) for x in self.ratio.split(":")]
        self.numerator = r[0]
        self.denominator = r[1]
        return self

    def __str__(self):
        return f"{self.ratio}"


def convert_ratio(ratios: dict or str, strength) -> Ratio:
    if isinstance(ratios, str):
        return Ratio(ratios).convert()
    return Ratio(ratios.get(strength)).convert()


def calculate_ingredients(coffee, maker, water):
    if coffee:
        maker.set_coffee(coffee)
    elif water:
        maker.set_water(water)
    else:
        maker.set_coffee(20)


def set_water_from_coffee(quantity: int, ratio: Ratio) -> int:
    return quantity * ratio.numerator * ratio.denominator


def set_coffee_from_water(quantity: int, ratio: Ratio) -> float:
    return round(quantity * ratio.numerator / ratio.denominator, 2)


class CoffeeMaker:
    def __init__(self, method: str = "brew", strength: int = 3, people: int = 1):
        self.method = method
        self.strength = strength
        self.people = people
        self.coffee = 20  # In gramme
        self.water = 300  # In millilitre
        self.ratio = convert_ratio(RATIOS[self.method], self.strength)

    def run(self, coffee: int = None, water: int = None, ratio: str = None):
        if coffee and water:
            new_ratio = int(water / coffee)
            self.set_ratio(f"1:{new_ratio}")
        elif coffee and not water:
            self.set_coffee(quantity=coffee)
        elif water and not coffee:
            self.set_water(quantity=water)
        if ratio:
            if coffee and not water:
                self.set_ratio(ratio=ratio).set_coffee(quantity=coffee)
            if water and not coffee:
                self.set_ratio(ratio=ratio).set_water(quantity=water)
        return self

    def set_coffee(self, quantity: int):
        self.coffee = quantity * self.people
        self.water = set_water_from_coffee(quantity, self.ratio) * self.people

    def set_water(self, quantity: int):
        self.water = quantity * self.people
        self.coffee = set_coffee_from_water(quantity, self.ratio) * self.people

    def set_ratio(self, ratio: str):
        self.ratio = Ratio(ratio=ratio).convert()
        return self

    @property
    def recipe(self):
        return {
            "method": self.method,
            "coffee": f"{self.coffee} gr.",
            "water": f"{self.water} gr.",
            "ratio": f"{self.ratio}",
            "spoons": round(self.coffee / 7, 1),
        }

    def get_table(self, output: bool = True):
        result = []
        for strength, ratio in RATIOS[self.method].items():
            r = Ratio(ratio).convert()
            result.append(
                {
                    "strength": strength,
                    "ratio": str(r),
                    "coffee": self.coffee,
                    "water": set_water_from_coffee(self.coffee, r),
                }
            )
        if output:
            pprint(result)
        return result
