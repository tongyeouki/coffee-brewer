import click

from brewer import CoffeeMaker
from usecases import (
    prepare_ristretto,
    prepare_expresso,
    prepare_longo,
    prepare_double,
    prepare_dishwater,
    use_moka,
    use_french_press,
    use_chemex,
    use_v60,
    prepare_americano,
    prepare_cold_brew,
)
from config import METHODS


def pprint(data):
    for key, value in data.items():
        click.echo(f"\t{key.capitalize()}\t: {value.capitalize() if isinstance(value, str) else value}")


@click.group()
def cli():
    """Console script for coffee brewer."""
    click.echo("Hello Barista!\n")


@cli.command("make")
@click.option("--method", "-m", default="brew", show_default=True, type=click.Choice(METHODS), help="Method")
@click.option("--strength", "-s", default=3, show_default=True, help="Strength: soft 1 -> 8 strong")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", required=False, help="Coffee beans in grammes", type=int)
@click.option("--water", "-w", required=False, help="Water in grammes", type=int)
@click.option("--ratio", "-r", required=False, help="Ratio in <int>:<int>", type=str)
def make(method, strength, people, coffee, water, ratio):
    """Pour coffee with custom options"""
    maker = CoffeeMaker(method=method, strength=strength, people=people)
    if coffee:
        click.echo(f"For {coffee} grammes of coffee : ")
        maker.set_coffee(quantity=coffee)
    elif water:
        click.echo(f"For {water} grammes of water : ")
        maker.set_water(quantity=water)
    if ratio:
        if coffee:
            maker.set_ratio(ratio=ratio).set_coffee(quantity=coffee)
        elif water:
            maker.set_ratio(ratio=ratio).set_water(quantity=water)
    pprint(maker.recipe)


@cli.group("prepare")
def prepare():
    """What kind of coffee do you like ?"""


@prepare.command("americano")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", required=False, help="Coffee beans in grammes", type=int)
@click.option("--water", "-w", required=False, help="Water in grammes", type=int)
def americano(people, coffee, water):
    result = prepare_americano(people=people, coffee=coffee, water=water)
    pprint(result)


@prepare.command("ristretto")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", default=18, show_default=True, required=False, help="Coffee beans in grammes", type=int)
def ristretto(people, coffee):
    result = prepare_ristretto(people=people, coffee=coffee)
    pprint(result)


@prepare.command("expresso")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", default=18, show_default=True, required=False, help="Coffee beans in grammes", type=int)
def expresso(people, coffee):
    result = prepare_expresso(people=people, coffee=coffee)
    pprint(result)


@prepare.command("longo")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", default=18, show_default=True, required=False, help="Coffee beans in grammes", type=int)
def longo(people, coffee):
    result = prepare_longo(people, coffee)
    pprint(result)


@prepare.command("double")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", default=18, show_default=True, required=False, help="Coffee beans in grammes", type=int)
def double(people, coffee):
    result = prepare_double(people, coffee)
    pprint(result)


@prepare.command("dishwater")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", required=False, help="Coffee beans in grammes", type=int)
@click.option("--water", "-w", required=False, help="Water in grammes", type=int)
def dishwater(people, coffee, water):
    result = prepare_dishwater(people, coffee, water)
    pprint(result)


@prepare.command("cold-brew")
@click.option("--coffee", "-c", required=False, help="Coffee beans in grammes", type=int)
@click.option("--water", "-w", required=False, help="Water in grammes", type=int)
@click.option("--french-press", required=False, is_flag=True, help="Use french press", type=bool)
@click.option("--concentrate", required=False, is_flag=True, help="Brew concentrate", type=bool)
def cold_brew(coffee, water, french_press, concentrate):
    result = prepare_cold_brew(coffee, water, concentrate, french_press)
    pprint(result)


@cli.group("use")
def use():
    """What kind of coffee maker do you use ?"""


@use.command("chemex")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", required=False, help="Coffee beans in grammes", type=int)
@click.option("--water", "-w", required=False, help="Water in grammes", type=int)
def chemex(people, coffee, water):
    result = use_chemex(people, coffee, water)
    pprint(result)


@use.command("moka")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--water", "-w", required=False, help="Water in grammes", type=int)
def moka(people):
    result = use_moka(people)
    pprint(result)


@use.command("french-press")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", required=False, help="Coffee beans in grammes", type=int)
@click.option("--water", "-w", required=False, help="Water in grammes", type=int)
def french_press(people, coffee, water):
    result = use_french_press(people, coffee, water)
    pprint(result)


@use.command("v60")
@click.option("--people", "-p", default=1, show_default=True, help="Number of people")
@click.option("--coffee", "-c", required=False, help="Coffee beans in grammes", type=int)
@click.option("--water", "-w", required=False, help="Water in grammes", type=int)
def v60(people, coffee, water):
    result = use_v60(people, coffee, water)
    pprint(result)
