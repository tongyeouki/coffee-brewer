from src.brewer import CoffeeMaker
from usecases import prepare_cold_brew, use_v60


def test_should_calculate_water_quantity_from_coffee():
    # Arrange
    sut = CoffeeMaker(strength=3, people=1)
    expected = {"method": "brew", "coffee": "20 gr.", "water": "300.0 gr.", "ratio": "1:15", "spoons": 2.9}
    # Act
    sut.set_coffee(20)
    # Assert
    assert sut.recipe == expected


def test_should_calculate_coffee_quantity_from_water():
    # Arrange
    sut = CoffeeMaker(strength=3, people=1)
    expected = {"method": "brew", "coffee": "20.0 gr.", "water": "300 gr.", "ratio": "1:15", "spoons": 2.9}
    # Act
    sut.set_water(300)
    # Assert
    assert sut.recipe == expected


def test_should_make_coffee_for_several_people():
    sut = CoffeeMaker(method="expresso", strength=6, people=2)
    expected = {"method": "expresso", "coffee": "36 gr.", "water": "72.0 gr.", "ratio": "1:2", "spoons": 5.1}
    # Act
    sut.set_coffee(18)
    # Assert
    assert sut.recipe == expected


def test_should_use_expresso_ratios():
    # Arrange
    sut = CoffeeMaker(method="expresso", strength=6, people=1)
    expected = {"method": "expresso", "coffee": "7 gr.", "water": "14.0 gr.", "ratio": "1:2", "spoons": 1.0}
    # Act
    sut.set_coffee(7)
    # Assert
    assert sut.recipe == expected


def test_should_generate_ratio_table():
    # Arrange
    sut = CoffeeMaker()
    expected = [
        {"strength": 1, "ratio": "1:17", "coffee": 20, "water": 340},
        {"strength": 2, "ratio": "1:16", "coffee": 20, "water": 320},
        {"strength": 3, "ratio": "1:15", "coffee": 20, "water": 300},
        {"strength": 4, "ratio": "1:14", "coffee": 20, "water": 280},
        {"strength": 5, "ratio": "1:13", "coffee": 20, "water": 260},
        {"strength": 6, "ratio": "1:12", "coffee": 20, "water": 240},
        {"strength": 7, "ratio": "1:11", "coffee": 20, "water": 220},
        {"strength": 8, "ratio": "1:10", "coffee": 20, "water": 200},
    ]
    # Act
    result = sut.get_table(output=False)
    # Assert
    assert result == expected


def test_should_print_ratio_table(capsys):
    # Arrange
    sut = CoffeeMaker()
    # Act
    sut.get_table(output=True)
    captured = capsys.readouterr()
    # Assert
    assert captured.out != ""


def test_should_use_custom_ratio_set_coffee():
    # Arrange
    sut = CoffeeMaker(people=1)
    expected = {"method": "brew", "coffee": "20 gr.", "water": "400.0 gr.", "ratio": "1:20", "spoons": 2.9}
    # Act
    sut.set_ratio("1:20").set_coffee(20)
    # Assert
    assert sut.recipe == expected


def test_should_use_custom_ratio_set_water():
    # Arrange
    sut = CoffeeMaker(people=1)
    expected = {"method": "brew", "coffee": "25.0 gr.", "water": "500 gr.", "ratio": "1:20", "spoons": 3.6}
    # Act
    sut.set_ratio("1:20").set_water(500)
    # Assert
    assert sut.recipe == expected


def test_should_prepare_v60():
    # Arrange
    expected = {"method": "v60", "coffee": "20 gr.", "water": "320.0 gr.", "ratio": "1:16", "spoons": 2.9}
    # Act
    recipe = use_v60(people=1, coffee=None, water=None)
    # Assert
    assert recipe == expected


def test_should_prepare_cold_brew_with_jar():
    # Arrange
    expected = {"method": "cold brew", "coffee": "200.0 gr.", "water": "1000 gr.", "ratio": "1:5", "spoons": 28.6}
    # Act
    sut = prepare_cold_brew(coffee=None, water=1000, concentrate=False, french_press=False)
    # Assert
    assert sut == expected


def test_should_prepare_cold_brew_with_french_press():
    # Arrange
    expected = {"method": "cold brew", "coffee": "142.86 gr.", "water": "1000 gr.", "ratio": "1:7", "spoons": 20.4}
    # Act
    sut = prepare_cold_brew(coffee=None, water=1000, concentrate=False, french_press=True)
    # Assert
    assert sut == expected


def test_should_prepare_concentrate_cold_brew():
    # Arrange
    expected = {"method": "cold brew", "coffee": "500 gr.", "water": "1000.0 gr.", "ratio": "1:2", "spoons": 71.4}
    # Act
    sut = prepare_cold_brew(coffee=500, water=None, concentrate=True, french_press=False)
    # Assert
    assert sut == expected


def test_should_change_ratio_and_choose_coffee():
    # Arrange
    sut = CoffeeMaker()
    # Act
    sut.set_ratio("1:40")
    # Assert
    assert str(sut.ratio) == "1:40"


def test_run_should_calculate_ratio():
    # Arrange
    sut = CoffeeMaker()
    # Act
    sut.run(coffee=25, water=500)
    # Assert
    assert str(sut.ratio) == "1:20"


def test_run_should_calculate_ratio2():
    # Arrange
    sut = CoffeeMaker()
    # Act
    sut.run(coffee=30, water=500)
    # Assert
    assert str(sut.ratio) == "1:16"


def test_run_should_get_coffee():
    # Arrange
    sut = CoffeeMaker()
    # Act
    sut.run(coffee=25, ratio="1:20")
    # Assert
    assert str(sut.ratio) == "1:20"
    assert sut.water == 500


def test_run_should_get_water():
    # Arrange
    sut = CoffeeMaker()
    # Act
    sut.run(water=500, ratio="1:20")
    # Assert
    assert str(sut.ratio) == "1:20"
    assert sut.coffee == 25
