# COFFEE BREWER

How to make a good coffee ?

If I use a v60, a French Press, if I want a tasty dishwater, what should be
the appropriate ratio between coffee beans and water ?

Answer in :

```bash
$ brewer use v60 -w 330
```

Output:

```
Hello Barista!

        Method  : V60
        Coffee  : 20.62 gr.
        Water   : 330 gr.
        Ratio   : 1:16
        Spoons  : 2.9
```

```bash
$ brewer prepare dishwater -c 20
```

Output:

```
Hello Barista!

        Method  : Brew
        Coffee  : 20 gr.
        Water   : 400.0 gr.
        Ratio   : 1:20
        Spoons  : 2.9
```

## Usage

```bash
$ brewer <command> --help
```

## Install

In a virtualenv (or not):

```bash
$ python3 -m venv venv
$ source venv/bin/activate
```

```bash
$ make install
```

## Developers

Almost everything is in the `Makefile`
