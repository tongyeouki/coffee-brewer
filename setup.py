#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
from os.path import basename
from os.path import dirname
from os.path import join
from glob import glob

from os.path import splitext

with open("README.md") as readme_file:
    readme = readme_file.read()

requirements = [
    "Click>=6.0",
]

setup_requirements = [
    "pytest-runner",
]

test_requirements = [
    "pytest",
]

setup(
    author="Tongyeouki",
    author_email="tongyeouki@gmail.com",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.9",
    ],
    description="How to make coffee",
    entry_points={
        "console_scripts": ["brewer=cli:cli"],
    },
    install_requires=requirements,
    long_description=readme,
    include_package_data=True,
    platforms="any",
    keywords="coffee-brewer, coffee",
    name="coffee-brewer",
    packages=find_packages("src"),
    package_dir={"": "src"},
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/tongyeouki/coffee-brewer",
    version="0.0.1",
    zip_safe=False,
)
