from brewer import CoffeeMaker, calculate_ingredients


def prepare_ristretto(people, coffee):
    maker = CoffeeMaker(method="expresso", strength=8, people=people)
    maker.set_coffee(coffee)
    return maker.recipe


def prepare_expresso(people, coffee):
    maker = CoffeeMaker(method="expresso", strength=6, people=people)
    maker.set_coffee(coffee)
    return maker.recipe


def prepare_longo(people, coffee):
    maker = CoffeeMaker(method="expresso", strength=2, people=people)
    maker.set_coffee(coffee)
    return maker.recipe


def prepare_double(people, coffee):
    maker = CoffeeMaker(method="expresso", strength=6, people=people)
    maker.set_coffee(coffee)
    return maker.recipe


def prepare_dishwater(people, coffee, water):
    maker = CoffeeMaker(strength=6, people=people)
    maker.set_ratio("1:20")
    calculate_ingredients(maker=maker, coffee=coffee, water=water)
    return maker.recipe


def prepare_americano(people, coffee, water):
    maker = CoffeeMaker(method="americano", people=people)
    calculate_ingredients(coffee, maker, water)
    return maker.recipe


def prepare_cold_brew(coffee, water, concentrate, french_press):
    strength = 6
    if french_press:
        strength = 4
    if concentrate:
        strength = 8
    maker = CoffeeMaker(method="cold brew", strength=strength, people=1)
    calculate_ingredients(coffee, maker, water)
    return maker.recipe


def use_chemex(people, coffee, water):
    maker = CoffeeMaker(strength=1, people=people)
    calculate_ingredients(coffee, maker, water)
    return maker.recipe


def use_v60(people, coffee, water):
    maker = CoffeeMaker(method="v60", strength=3, people=people)
    calculate_ingredients(coffee, maker, water)
    return maker.recipe


def use_moka(people):
    maker = CoffeeMaker(method="brew", strength=8, people=people)
    maker.set_coffee(20)
    return maker.recipe


def use_french_press(people, coffee=None, water=None):
    maker = CoffeeMaker(method="brew", strength=3, people=people)
    calculate_ingredients(coffee, maker, water)
    return maker.recipe
